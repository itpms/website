---
title : "IT-Projekt Münster e.V."
description: "Digitale Bildung, Medienkompetenz und MINT Interesse fördern!" 
lead: "Digitale Bildung, Medienkompetenz und MINT Interesse fördern!" 
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---

![](intro_img.jpg)
Hier informieren wir über unser überschulisches IT-Projekt Münster. Da wir mit unserer regionalen und überschulischen Initiative das Ziel verfolgen, Schülerinnen und Schüler in den Bereichen MINT (Mathematik, Informatik, Naturwissenschaft und Technik) Digitalisierung und Medienbildung zu fördern, konzentrieren sich unsere Aktivitäten seit 2006 auf das I und T von MINT. IT steht dabei für Informatik und Technik und bildet unseren Schwerpunkt.

Getragen wird das Projekt aktuell u.a. von Lehrkräften des Annette-von-Droste-Hülshoff-Gymnasiums Münster, dem Schillergymnasium, Gymnasium Wolbeck, IT-Enthusiasten, Studenten und Lehrkräften der Informatik und verschiedenen Vereinen, die die Initiative finanziell unterstützen. Wo es möglich ist, kommen OpenSource Software und kostengünstige Hardware zum Einsatz! In kleinen Gruppen werden die Schülerinnen und Schüler in die Themenbereiche eingewiesen. Das Spektrum der Projekte beginnt bereits in der 2. Klasse der Grundschule und reicht bis in den Abiturbereich hinein. Im Moment erfolgt die Wissensvermittlung durch Studenten der Informatik und Lehrkräfte sowie Personen aus dem IT – Bereich. Ehemalige Schülerinnen und Schüler schlüpfen dann selbst in die Lehrerrolle und so entsteht nachhaltig ein cascadierendes Tutorenprinzip (Schneeballprinzip). Dabei werden die Schülertutoren methodisch und didaktisch fortwährend durch Lehrer, Referendare und Studenten unterstützt.
