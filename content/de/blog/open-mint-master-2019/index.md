---
title: "Open-Mint-Master 2019"
description: "OPEN-MINT-MASTERS-Wettbewerb"
lead: ""
date: 2022-05-04
draft: false
weight: 60
images: []
contributors: ["IT-Projekt Münster e.V."]
---

Wettbewerb für Robotik, Informatik, Technik, NW, 3D Druck, Mathematik, NRW, Münster, Münsterland, MINT FörderungDer Open MINT Master – Wettbewerb für Jugendliche

Ende 2014 wurde die Idee, eine offenen MINT Meisterschaft zu entwickeln, geboren. Die OPEM-MINT-MASTERS Meisterschaften (mit den aktuell fokussierten Bereichen Informatik, Technik/Robotik) gehen 2019 nun in die fünfte Saison. Der Wettbewerb zur Förderung MINT-interessierter Schülerinnen und Schüler findet in diesem Jahr am Gymnasium Wolbeck (Von-Holte-Straße 56, 48167 Münster) statt.

### Phase 1:
[Das Regelwerk und die Aufgaben für den Open MINT MASTERS Wettbewerb](Regelwerk-1.pdf)

### Phase 2:
Die Anmeldung zum Wettbewerb (2019) ist leider abgeschlossen. Für Rückfragen könnt Ihr uns gerne mailen.

### Phase 3: Tipps und Antworten:

#### Tips für Aufagbe 1

{{< video webm-src="/blog/open-mint-master-2019/aufgabe1_take2230a.mp4" ratio="16x9" attributes="controls " >}}

#### Tips für Aufagbe 2

{{< video webm-src="/blog/open-mint-master-2019/aufgabe3_futter_einlagern2e6f8.mp4" ratio="16x9" attributes="controls " >}}

### Regeln

Maximal 8 Teams (plus Nachrückliste) aus weiterführenden Schulen (je Team 3-6 Schüler, max. 2 Teams je Schule, die TeilnehmerInnen müssen aus den Klassenstufe 5-12 sein) werden sich in einzigartigen Disziplinen miteinander messen.

Bei dem diesjährigen Wettbewerb sind Schulen aus dem Regierungsbezirk Münster teilnahmeberechtigt.

Erlaubt ist (fast) alles. Lediglich Größe und Gewicht der Roboter sind beschränkt.
Es darf u.a. Roboterhardware von Arduino, Lego oder Fischertechnik genutzt werden, welche IDE / Programmierumgebung jedoch genutzt wird, steht den Teams frei.
Eigenkreationen sind ausdrücklich erlaubt und erwünscht!

Der Wettbewerb findet am Samstag, 06.04.2019, von 9 bis ca. 17 Uhr, in der Aula und weiteren Räumen des Gymnasium Wolbecks

Dabei kooperiert das Gymnasium Wolbeck mit dem Annette Gymnasium Münster und dem überschulischen MINT-Förderprojekt für Mädchen und Jungen „IT-Projekt-Münster„und vielen weiteren Partnern.

Der OPEN-MINT-Masters-Wettbewerb ist ein Mitbring-Event: Jedes Team trägt entsprechend seiner Teamgröße mit kleinen Speisen und Getränken zu dem großen Mitbring-Buffet bei, sodass ein großes und vielfältiges Angebot entsteht. Ebenso bringt jeder eigenes Geschirr und Getränkebecher mit.

Um die Kosten für die Schüler möglichst gering zu halten, ist das IT-Projekt-Münster zusammen mit den Kooperationspartnern noch immer auf der Suche nach Förderern.

Bei Anmeldebestätigung sind 25 Euro im Voraus zu entrichten.
