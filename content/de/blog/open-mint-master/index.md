---
title: "Open-Mint-Master 2020"
description: "OPEN-MINT-MASTERS-Wettbewerb"
lead: ""
date: 2022-05-04
draft: false
weight: 50
images: []
contributors: ["IT-Projekt Münster e.V."]
---

Der Wettbewerb wurde verschoben auf einen Samstag nach „Corona / COVID-19“! Alle weiteren Infos über den ITPMS-NEWSLETTER.

Das Wettbewerbsthema 2020:\
**Aufbruch ins All – Die Mars Weltraummission“**

Ende 2014 wurde die Idee, eine offenen MINT Meisterschaft zu entwickeln, geboren. Die OPEM-MINT-MASTERS Meisterschaften (mit den aktuell fokussierten Bereichen Informatik, Physik, Technik/Robotik) gehen 2020 nun in die sechste Saison. Der Wettbewerb zur Förderung MINT-interessierter Schülerinnen und Schüler findet in diesem Jahr im Herzen des Münsterlandes (in Münster in NRW) statt.

Mehrere Teams aus allen Schulformen (je Team 3-8 Schüler, max. 2-3 Teams je Schule – abhängig von der Schulgröße) werden sich in einzigartigen Disziplinen miteinander messen.

Bei dem diesjährigen Wettbewerb sind Schulen aus dem Regierungsbezirk Münster teilnahmeberechtigt.

Erlaubt ist (fast) alles. Lediglich Größe und Gewicht der Roboter sind beschränkt.
Es darf u.a. Roboterhardware von Arduino, Lego oder Fischertechnik genutzt werden, welche IDE / Programmierumgebung genutzt wird, steht den Teams frei.
Eigenkreationen sind ausdrücklich erlaubt und erwünscht!

Neben den wohlbekannten „Standardaufgaben“ müssen sich die Roboter in spannenden Aufgaben beweisen! Ebenso gibt es eine Wettbewerbsaufgabe mit einer 3-D Konstruktionsaufgabe: Hier sollen Bauteile selbst entwickelt werden.

Der Wettbewerb findet am Samstag, 28.03.2020, von 10 bis ca. 17 Uhr, in den Räumen der Stadtbücherei Münster statt.

Bei Wettbewerbsanmeldung fallen pro Team Teilnahmegebüren in Höhe von 25 € an.

Dabei kooperiert das Annette Gymnasium Münster, dem Gymnasium Wolbeck und dem überschulischen MINT-Förderprojekt für Mädchen und Jungen „IT-Projekt-Münster“ und vielen weiteren Partnern.

Material und Regeln:
Regelwerk
Plakat für Eure Klasse

