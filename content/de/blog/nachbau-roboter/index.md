---
title: "Nachbau Roboter"
description: "Unser Nachbau Roboter"
lead: ""
date: 2022-05-04
draft: false
weight: 50
images: ["robot_protoype_3.jpg"]
contributors: ["IT-Projekt Münster e.V."]
---

![Prototyp des Roboter](robot_protoype_3.jpg)

## Das Internet ist voll von Robotern - oder auch nicht

Bei der Suche nach einer geeigneten und erschwinglichen Lern-Roboterplattform für unsere Wettbewerbe und die Teams haben wir bemerkt, dass es der Markt aktuell im Grunde wie leergefegt ist.

Viele etablierten Hersteller haben sich zurückgezogen (vermutlich wegen des Preisdrucks durch den asiatischen Markt) und neue "Player" am Markt verschwinden so schnell wie sie aufgetaucht sind - und mit ihnen die Ersatzteile, Ergänzungsmodule und auch die Ideen.\
Und so steht man all zu oft mit einem tollen Roboterchassis da - und kann nichts mehr damit anfangen.\
Bei den wenigen übrig gebliebenen Anbietern sind die Preise buchstäblich durch die Decke gegangen weil sich halt der Markt auf wenige Anbieter konzentriert hat.

### Bauanleitungen, Bestellisten und 3D-Modelle "for free"

Also haben wir die Ärmel hochgekrempelt und unser Roboter-KnowHow zusammengeworfen.

Rausgekommen ist ein erstes Robotermodell in der Preisklasse um die 30 - 50 €. Plus etwas Bastelzeit und etwas Geduld für den 3D-Druck.

Wir haben aus der Grundidee einen modularen, frei programmierbaren und erweiterbaren Roboterbausatz entwickelt und vor allen Dingen allen Phasen des Zusammenbaus ausgiebig getestet und so lange daran gefeilt, bis der Aufbau im Grunde von jeder und jedem "geschafft" werden kann.

![Verdrahtung des Roboers](prototyp1_20220316_detailverdrahtung.jpg)

Dabei habne wir soweit irgend möglich auf Löten verzichtet und ein Steckersystem konzipiert dass auch für eigene Erweiterungen passt.

Und natürlich gibt es allerlei vorgefertigte Programme und Programmbibliotheken die jede und jeder selbst verändern und anpassen kann - und soll :-)

### Programmierung

Vorgegebene oder selbst erdachte Aufgaben mit einem Roboter zu lösen ist ja die Kernidee unserer Roboterprojekte und natürlich auch der Roboterwettbewerbe (die wir wegen Corona aktuell ausgesetzt haben). Darum ist eine freie, kostenlose und leicht bedienbare Programmierumgebung für uns genauso wichtig wie die frei zugänglichen Baupläne.\
Die Programmierung kann über beliebige Programmiertools gemacht werden. Wir haben uns für "Arduino" und "Scratch" (bzw. "Blockly") entschieden und die Beispielprogramm darauf abgestimmt.

### Zusammen voneinander lernen

Für viele Roboterbegeisterte ist der Selbstbau trotz Anleitung und bunten Bildern allerdings schon ein ziemlich dickes Brett - bei dem man dann schnell die Lust zu verliert.\
Und damit das nicht passiert gibt es von uns Workshops und Kures. Online oder als Präsenz und, wenn irgend möglich, kostenlos bzw. zum Selbstkostenpreis (für das Baumaterial)

### Roboterwettbewerb als großes Finale

Und irgendwann, vielleicht nach einem halben Jahr, treffen sich alle Baumeister und alle Teams zu den "Open Mint Masters Münster" und lassen die Roboter miteinander um die besten Pläzte wetteifern.


### Demo
{{< video webm-src="/blog/nachbau-roboter/prototyp3_20220316_turn1.ogg" ratio="1x1" attributes="controls autoplay muter" >}}
