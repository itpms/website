---
title: "ITPMS-Archiv"
description: "Unser Archiv"
lead: ""
date: 2022-05-04
draft: false
weight: 50
images: ["robot_protoype_3.jpg"]
contributors: ["IT-Projekt Münster e.V."]
---

# 2019

## HOUR OF CODE-Workshops: Scratch VR Spiele und Arduino Spiel 2019
  ![](images/img_2478_01-scaled.jpg)
  ![](images/img_2461-scaled.jpg)
- EU Code Week
- Unterstützung der Jugend forscht Gruppe „Anchar„. Einen herzlichen Grückwunsch zum Erfolg beim Regional-Wettbewerb in Münster.(s. Post bei den Unterstützern Annette Gymnasium, Phoenix Contact EMobility, etc.)

# 2018

- HOUR OF CODE-Workshops für Schülerinnen und Schüler. ![Artikel in münstersche Zeitung](images/lernen-40-digitalisierung-am-annette.jpg)
- Unterstützung der Schülerakademie SMIMS u.a. Anleitungen für das Netzwerk und den neuen Flyer für wissenschaftliche Veröffentlichungen mit dem Schriftsetzungsprogramm LaTEX, zu finden [hier](https://old.it-projekt-muenster.de/wp-content/uploads/2018/12/einfuehrung.pdf)
- Open MINT MASTERS-Wettbewerb 2018 am Annette-von-Droste-Hülshoff-Gymnasium Münster
- IT-Projekt-Münster im Radio:

# 2017
-  HOUR OF CODE-Workshop: Arduino 2017
  ![](images/2017-hour-of-code.jpg)
  ![](images/2017-hour-of-code2.jpg)
  ![](images/2017-hour-of-code-1.jpg)

# 2016

- OPEN-MINT-Masters 2017
  [Flyer](https://old.it-projekt-muenster.de/wp-content/uploads/OMM/Flyer-OPEN-MINT-MASTER-2016-ROBOTERWETTBEWERB-M%c3%9cNSTER.pdf)
  [Plakat](https://old.it-projekt-muenster.de/wp-content/uploads/OMM/Flyer-OPEN-MINT-MASTER-2016-ROBOTERWETTBEWERB-M%c3%9cNSTER.pdf)
- OPEN-MINT-Master 2016 <br>
Die regionalen Stadtmeisterschaften für Informatik und Technik (Schwerpunkt Robotik) gehen 2016 in die zweite Runde. Der Wettbewerb zur Förderung MINT-interessierter Schülerinnen und Schüler findet in diesem Jahr erneut am Annette Gymnasium in Münster statt.Maximal 12 Teams aus weiterführenden Schulen (je Team 3-6 Schüler, max. 2 Teams je Schule, die Teilnehmer müssen aus dem Geburtsjahr 1998 (oder jünger) sein.) werden sich in einzigartigen Roboter-Disziplinen miteinander messen.Bei dem diesjährigen Wettbewerb sind Schulen aus maximal 20 km Umkreis um Münsterteilnahmeberechtigt.Erlaubt ist (fast) alles. Lediglich Größe und Gewicht der Roboter sind beschränkt.
-  Es darf u.a. Roboterhardware von Arduino, Lego oder Fischertechnik genutzt werden, welche IDE / Programmierumgebung jedoch genutzt wird, steht den Teams frei.
Eigenkreationen sind ausdrücklich erlaubt und erwünscht!
Neben den wohlbekannten „Standardaufgaben“ müssen sich die Roboter in einem Dragster-SpeedRace und einem RC-Race beweisen – im Gegensatz zu den meisten anderen Challanges gilt hier also „Fernsteuern erlaubt“!
- Die Regeln:
  -  Der Wettbewerb findet am Samstag, 18.6.2016, von 9 bis 17 Uhr, in der Aula und weiteren Räumen des  Annette-von-Droste-Hülshoff-Gymnasium Münster, Grüne Gasse 38, 48143 Münster, statt. (Bitte die öffentlichen Parkplätze und Parkhäuser nutzen.)Dabei kooperiert das Annette Gymnasium Münster mit dem überschulischen MINT-Förderprojekt für Mädchen und Jungen „IT-Projekt-Münster“ und vielen weiteren.
Der OPEN-MINT-Masters-Wettbewerb 2016 ist ein Mitbring-Event: Jedes Team trägt entsprechend seiner Teamgröße mit kleinen Speisen und Getränken zu dem großen Mitbring-Buffet bei, sodass ein großes und vielfältiges Angebot entsteht. Ebenso bringt jeder eigenes Geschirr und Getränkebecher mit.>>> Interessierte können sich hier für den Newsletter anmelden 

# 2015

- First Lego League 2014 / 2015 in MünsterDer Regionalwettbewerb der First Lego League 2014 – 2015 findet 2015 am Annette-von-Droste-Hülshoff-Gymnasium Münster, Grüne Gasse 38, 48143 Münster statt.SEI MIT DEINEM TEAM DABEI: http://www.first-lego-league.org/

# 2014

- Angebot von ECDL / Computerführerschein NRW AG und Informatik / Robotik AG

# 2013

- Die Idee der IT-Stadtmeisterschaft(en) ist geboren: ITPMS-Finals am Annette zwischen dem Schiller und dem Annette.The beginning
- Angebot von ECDL / Computerführerschein NRW AG und Informatik / Robotik AG

# 2012

- ZDI Roboter Wettbewerb 2012 – Eine reife Leistung
Es surrte wieder in den Vorlesungssälen der Universität Münster. Nach mehreren Monaten Vorbereitungszeit trafen sich Teams aus ganz NRW drei Tage lang im großen Physik-Vorlesungssaal an der Universität Münster.Durch den Impuls des IT Projekts Münster nahmen die drei Kooperationsschulen, das Annette Gymnasium Münster, das Schiller-Gymnasium Münster und das Gymnasium Wolbeck, am diesjährigen Roboter Wettbewerb teil. Im sportlichen Wettstreit mit den anderen angemeldeten Gruppen galt es sich im überschulischen Wettbewerb zu messen. Für die zahlreichen kniffeligen Aufgaben gab es insgesamt eine Zeitvorgabe von 2:30 Minuten. Über drei Durchläufe mussten sich die Teams die maximale Punktzahl erarbeiten um bis ins Halbfinale zu gelangen. Ein ganzes Stück Programmierarbeit mit den kleinen, wilden Robotern. Neben den vorher bekannten Aufgaben gab es zusätzlich noch eine geheime Aufgabe, die die Teams innerhalb weniger Minuten vor den Augen der Jury alleine lösen mussten. Sie war recht kniffelig aber durch Kreativität und den Teamgeist der Gruppen irgendwie machbar. Nach mehreren Stunden in denen die Teams gegeneinander antreten mussten, kam es dann zum spannenden Showdown. Überraschend zog ein Team des Annette Gymnasiums ins Halbfinale ein. In packenden 2:30 gab das Team mit Ihrem Roboter alles, um möglichst viele Punkte zu sammeln. Dabei surrten die Motoren durch den Raum, das Publikum fieberte mit und die Mühe der Vorbereitung zahlte sich letztendlich aus. Es gelang dem Team der erfolgreiche 3. Platz!
![](images/2012-zdi.jpg)
- Das Team des Gymnasium Wolbeck belegte einen guten Platz im Mittelfeld,
das Team des Schiller-Gymnasiums den 5. Platz und die Teams „Das A-Team“
und das Team „MINT Master“ des Annette Gymnasiums fordere Plätze. Somit erzielte das IT-Projekt die bislang erfolgreichsten Gesamtplatzierungen. Eine respektable Leistung, besonders für die Schülerinnen und Schüler die zum ersten Mal bei einem Wettbewerb dabei waren. „Wir freuen uns schon auf nächstes Jahr“, da waren sich alle Teilnehmerinnen und Teilnehmer einig!Mit Unterstützung der Stadt Münster und der Rütergs Stiftung nahmen 5 Teams von drei Kooperationsschulen am Roboter-Wettbewerb „ZDI“ teil.Die Aktion IT-Projekt-Münster hilft bei der Förderung von MINT Projekten schnell und unbürokratisch und unterstützt dabei Schülerinnen und Schüler, sowie deren Lehrerinnen und Lehrer, ihre Ideen zu realisieren.
- Ein herzliches Dankeschön an all unsere Sponsoren und Unterstützter. Mit Ihrer Hilfe konnten wir beim ZDI Roboter Wettbewerb in Münster wieder mit mehreren Teams von drei Schulen starten. Besonders hervorheben möchten wir die Unterstützung der Stadt Münster und der Rütgersstiftung, die es über das IT-Projekt-Münster naturwissenschaftlich interessierten Schülerinnen und Schülern ermöglichen, an unseren Projekten teilzunehmen.
Ihnen gilt unserer besonderer Dank.
- Zwei Wettbewerbe in 48 Stunden:
   Das Schuljahr neigt sich dem Ende entgegen und damit zeichnet sich auch der Saisonabschluss bei den Roboter Wettbewerben an. Doch vor den Sommerferien nahmen wir an der Robo-Com an der Fachhochschule Gelsenkirchen und am Robo-Tec in Osnabrück teil.
Jeweils ein Team vertrat dabei das IT-Projekt, das Annette Gymnasium und das Schiller-Gymnasium.Am Donnerstag startet das erste Team um 6:30 quer durch das Ruhrgebiet zu Fachhochschule Gelsenkirchen, die nun auch Westfälische Hochschule genannt wird. Nachdem die Rush Hour im Ruhrgebiet überstanden war und das Ziel dank GPS gefunden wurde, meldete sich das Team am Wettkampfbüro an. Dort angekommen wurden die Wettbewerbsunterlagen übergeben und das Team erprobte die theoretisch gestellten Aufgaben erstmalig in der Praxis. Die Unterschiede in der Ausführung waren so groß, dass Roboter und Programmierung noch stark überarbeitet wurden und das Team in der Zeit bis zum ersten Durchlauf auf Hochtouren arbeitete. Dann wurden alle Roboter im „Park Fermé“ abgestellt und durften nicht weiter verändert werden. Bei den Roboter-Checks der Jury zusammen mit Prof. Kluge (der Veranstalter der Robo-Com) im Park Fermé wurden verschiedene Regelverstöße (unerlaubte Bauteile etc.) entdeckt, wodurch einige Teams schon im Vorfeld disqualifiziert wurden.
  In den Läufen schauten sich dann alle Teams die Konkurrenz an und fieberten mit. Es galt einer Linie über einen längeren Weg zu folgen und dann auf einem freien Feld einen Ball zielgenau in ein Loch zu werfen. Dabei spielte die Schnelligkeit des Roboters die entscheidende Rolle. Trotz eines präzise fahrenden Roboters reichte die Zeit am Ende nur für einen guten Platz im Mittelfeld des Wettbewerbs. Dennoch hatte das Team mit Mirja, Felix und Lucas eine gute Zeit in Gelsenkirchen, die spätabendliche Arbeitsphase am Vortag hatte sich gelohnt.Am nächsten Morgen um 7:00 Uhr fuhren Lucas, Leo und ihr Betreuer Herr Büdding zur Robo-Tec nach Osnabrück. Dort angekommen liefen sie bei schönstem Sonnenschein zum imposanten KME Gelände und wurden von einer Schar Auszubildender in Empfang genommen. Nach einer schnellen Anmeldung bekam das Team von einem Mitarbeiter aus dem Wettkampfbüro einen Arbeitsraum zugewiesen. Hier konnten die einzelnen Teams ihre Systeme vorbereiten und verbessern, es standen Übungstische für Probeabläufe bereit und die Roboter konnten immer wieder getestet werden – unter realen Bedingungen.
  Bis zur letzen Minute wurde an den verschiedensten Problemen gefeilt. Nach jeweils zwei Durchläufen in den verschiedenen Disziplinen sowie einem Postervortrag vor einer Jury warteten alle TeilnehmerInnen und BetreuerInnen auf die Auswertung. Mit einem guten dritten Platz verabschiedete sich das Team und fuhr zurück nach Münster. Alles in Allem ein gelungener Saisonabschluss.
  - Bericht über die IT Projekt Münster – Teilnahme am Kreuzviertelfest in Münster (x4telzeitung.de), über die Digitalisierungsbestrebungen des IT Projekt Münster, die Kreuzviertler Geschäftsleute und den Förderverein des Schillergymnasiums.


# 2011

- Stadt Münster fördert das IT-Projekt-Münster
Seit mehr als 5 Jahren arbeitet das IT Projekt im Bereich der schulischen M.I.N.T.-Förderung. Nun wird das IT Projekt von der Stadt Münster unterstützt. Der Rat der Stadt Münster sah unser Projekt „Mehr Informatik und Robotertechnik an Schulen“ als förderungswürdig an. So können wir unsere Aktivitäten 2012 ausweiten und freuen uns sehr darüber.
-  Neue AGs im Schuljahr 2011/2012 für Mädchen und Jungen! 
Das neue Schuljahr ist gestartet und wie in den vergangenen Jahren seit 2005 bieten wir Euch wieder verschiedenen AGs zu verschiedenen IT-Themen an verschiedenen Schulen im Raum Münster an.Falls Ihr Interesse an Vorträgen oder weiteren Workshops und Fortbildungen zum Themengebiet „Informatik, Mediennutzung und Internetsicherheit“ habt, helfen wir auch gerne weiter. Sei dabei, denn rein schauen lohnt sich!

- Sommerferien = Sommerpause?
Wie in den vergangenen Jahren besteht für MINT-interessierte Kids wieder die Möglichkeit an den Freizeiten für Kinder in den Sommerferien 2011 im „WersePark Sudmühle“ teilzunehmen.

- ZDI-Roboter-Wettbewerb 2011

Das IT-Projekt nahm auch in diesem Jahr wieder am ZDI Roboter Wettbewerb erfolgreich teil. Die Schülerinnen und Schüler der münsterländer Schulen waren wieder mit viel Engagement und Spass bei der Sache.

- AG-Angebote im 2. Halbjahr 2010/2011

Vorbemerkung: Geschlechtsspezifischer Unterricht für Mädchen und Jungen ist in aller Munde doch wie sieht es in der Realität bei uns im IT-Projekt-Münster aus? Die Mädchenförderung wird in unseren gemischten Kursen „groß“ geschrieben. Um insbesondere das Interesse der Schülerinnen zu wecken, werden spezielle Inhalte, die verstärkt Schülerinnen ansprechen behandelt. So lernen die Schülerinnen ihre eigenen Roboter nach ihren kreativen Ideen zu bauen und sie künstlerisch so zu gestalten, dass ihre Roboter zu SchauspielerInnen auf der Bühne werden. Dafür bekommen die Roboter Regieanweisungen, die die Schülerinnen via einfacher Programmierung an ihre Roboter weitergeben. So entsteht in der Gruppe ein kreatives Zusammenspiel von verschiedenen Robotern. Ziel ist die Förderung der Interesse der Schülerinnen an Robotik, Technik und Informatik. Dabei sollen sie bei Interesse fit für regionale und überregionale Roboter-Wettbewerbe werden.
Falls Du Lust hast, bei uns mitzumachen oder Fragen zu den Kursen hast, wenn Sie als Elternteil Ihre Tochter / Ihren Sohn bei uns anmelden möchten, oder wenn Sie als Schule mit dem IT-Projekt kooperieren möchten, dann melde dich / melden Sie sich bei uns:.

# 2010

- Präsentation auf dem Rütgers-Stiftungstag

Neugier fördern, Wissenshunger stillen.Am Freitag Nachmittag fand der Rütgers-Stiftungstag auf der Zeche Zollverein statt.

Fliegende Drohnen und Raketenautos bewegen sich durch die Halle der Zeche Zollverein.

Schülerinnen und Schüler zeigen auf dem Markt der Möglichkeiten ihre Forschungsergebnisse des letzten Jahres und die Lehrerinnen und Lehrer informieren sich über die Projekte der unterschiedlichen Fachrichtungen. Insgesamt 20 Schulprojekte stellten sich am Stiftungstag der Rütgers-Stiftung mit ihren geförderten Projekten vor und standen der Jury und dem breiten Publikum Rede und Antwort. Darunter auch das IT-Projekt Münster Team, welches ihr schul- und stufenübergreifendes Konzept vorstellte. Die Schülerinnen und Schüler berichteten von ihren Erfahrungen aus den Bereichen Informatik und Technik und zeigten die Schritte von der Modellierung und Konstruktion bis zur Programmierung der Roboter. Dazu berichteten sie in einer multimedialen Präsentation über ihre Erfahrungen in den AGs und Wettbewerben.
„Wir wollen die Neugier für Informatik und Robotik fördern und den Wissenshunger der Schülerinnen und Schüler stillen“, so Hendrik Büdding und Robert Nünning, die Leiter des IT-Projektes, einstimmig. „Die Schülerinnen und Schüler der verschiedenen Grund-, Realschulen und Gymnasien möchten ihr Wissen im Bereich der Informatik-Systemen erweitern und die eingesetzten Systeme nach ihren Wünschen und Vorstellungen programmieren und zur Lösung von Alltagsproblemen anwenden. Wir zeigen ihnen, wie es funktioniert, ihre Ideen realisieren die Kids dann eigenständig, so ihr Coach Julia Aldehoff. Ihrer Kreativität sind dabei keine Grenzen gesetzt, außer das Material zum Bauen geht uns aus. Den Schülerinnen und Schülern hat es sehr viel Spaß gemacht und sie wollen auch im neuen Jahr wieder mitmachen und die Welt mit ihren Robotern bewegen.“

- Kreuzviertelfest 2010

Wie in den vergangenen Jahren präsentierte das IT-Projekt-Münster Ihre Arbeiten an den Münsteraner Schulen auf dem zweitägigen Kreufviertelfest.
Das warme Sommerwetter zog besonders am Sonntag viele Besucher ins münsteraner Kreuzviertel. Die Erwachsenen bestaunten die Roboterkreationen der Schülerinnen und Schülern und die Erfolge auf den Roboterwettbewerben. Die jugendlichen Besucher konnten unter der Anleitung der Tutoren bei den Mitmachaktionen selbst Roboter bauen und programmieren. Dabei bereitete den Schülerinnen und Schülern der Einsatz eines Roboters, der mit einer Funkkamera ausgestattet war, besonderen Spaß. An einer anderen Station konnten die interessierten Schülerinnen und Schülern selbst kleine Programme mit der Programmieroberfläche Scratch programmieren.

- ZDI-Roboter-Wettbewerb 2010 in Münster

In diesem Jahr gingen zwei Teams erfolgreich an den Start beim zdi-Roboterwettbewerb 2010.

# 2009

- Entwicklung der pädagogischen, ikonischen Programmieroberfläche TuxMinds. Supports NxT, Asuro, Arduino-Bot and many more. With simulator.

# 2008/2007

- AG-Angebote zum Thema Mobile Medien im Informatik-Unterricht an verschiedenen münsteraner Gymnasien zusammen mit der WWU und der DDI (Didaktik der Informatik, Westfälische Wilhelms Universität Münster)
- Idee des IT Projekt Münster entsteht

# 2006/2007

- Geburt einer Idee: Interview im Antenne TUX



