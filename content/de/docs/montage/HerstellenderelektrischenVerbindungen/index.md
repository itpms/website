---
title: "Herstellen der elektrischen Verbindungen"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 70
---
{{< alert context="danger" text="Der Akku ist dabei noch nicht eingesetzt. Der USB-Programmieranschluß ist nicht verbunden. Der Roboter hat also noch keinen Strom ! Der Roboter steht so, das er von uns weg fahren würde!" />}} 

Nach der Installation können die elektronischen Komponenten miteinander verbunden werden.

1. Das Akkukabel mit der Sammelschiene verbinden:
Steckplatz links, grüne Ader in der Mitte, rote Ader unten.
2. Den Anschluss „-“ des Motortreibers mit einem grünen Jumperkabel mit der Sammelschiene (Mitte)  verbinden.
3. Den Anschluss „+“ des Motortreibers mit einem roten Jumperkabel mit der Sammelschiene (Unten)  verbinden.
4. Das Prozessorboard mit der Sammelschiene verbinden: „GND“ mit einem grünen Jumperkabel mit der  mittleren Schiene, „Vin“ mit einem roten Jumperkabel mit der unteren Schiene
5. Das Kabel des linken Motors mit den Ausgängen „MOTOR-A“ des Motortreibers verbinden.
    - „Motor Gelb“: Oberer Anschluss des Motors mit dem oberen Anschluss „MOTOR-A“.
    - „Motor klein“: Linker Anschluss des Motors mit dem oberen Anschluss „MOTOR-A“.
6. Das Kabel des rechten Motors mit den Ausgängen „MOTOR-B“ des Motortreibers verbinden.
    - „Motor Gelb“: Oberer Anschluss des Motors mit dem unteren Anschluss „MOTOR-B“.
    - „Motor klein“: Linker Anschluss des Motors mit dem unteren Anschluss „MOTOR-B“.
7. Das Prozessorboard mit den Eingängen des Motortreibers verbinden
    - D2 mit IN1 (blaues Jumperkabel)
    - D3 mit IN2 (lila Jumperkabel)
    - D4 mit IN3 (braunes Jumperkabel)
    - D5 mit IN4 (gelbes Jumperkabel)
