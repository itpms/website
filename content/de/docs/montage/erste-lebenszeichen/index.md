---
title: "Erste Lebenszeichen"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 80
---

Nach zweifacher Überprüfung der Verdrahtung kann der Roboter zum ersten Mal „unter Strom gesetzt“ werden.
Wichtig: An der Sammelschiene sind in der Mitte nur grüne und unten nur rote Adern angeschlossen !
Die Räder werden erst später montiert !
Der Roboter wird über das USB-Programmierkabel an den Computer angeschlossen.
Nach wenigen Sekunden startet das Testprogramm.

## Testprogramm

Zwischen den einzelnen Testschritten liegt jeweils eine Pause von zwei Sekunden.

1. Die LED auf dem Prozessorboard blinkt dreimal.
2. Der linke Motor dreht sich immer schneller in Richtung „vorwärts“ (gegen den Uhrzeigersinn \*)) und stoppt dann.
3. Der linke Motor dreht sich immer schneller in Richtung „rückwärts“  (im Uhrzeigersinn \*)) und stoppt dann.
4. Der rechte Motor dreht sich immer schneller in Richtung „vorwärts“ (gegen den Uhrzeigersinn \*)) und stoppt dann.
5. Der rechte Motor dreht sich immer schneller in Richtung „rückwärts“ (im Uhrzeigersinn \*))  und stoppt dann.
6. Das Testprogramm beginnt wieder mit Schritt 1.

\*) mit Blick von Außen auf die Antriebswelle des Motors

Nach erfolgreichem Test kann die Verbindung zum PC / Laptop getrennt werden und der Akku eingesetzt werden (mit der Kontrollleuchte nach oben) ggf. muss die Öffnung etwas entgratet und aufgefeilt werden.
Das Testprogramm sollte auch mit dem Akku fehlerfrei laufen.
Nun können die Räder und der Bodengleiter montiert werden und mit dem Testprogramm eine erste Probefahrt (auf dem Boden!) gestartet werden.
