---
title: "Akkuverbindung"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 20
---
Die Verbindung zum Akku erfolgt über den USB-Anschluß des Akkus. Dazu löten wir an den USB-Stecker zwei ca. 10 cm lange Drähte an (rt und gn !) An das andere Kabelende kommen wieder Einzelstecker. Beide Einzelstecker dürfen gerne zusammenbleiben denn die Stecker werden ohnehin mit der Sammelschiene verbunden.

Am USB-Stecker ist eine sorgfältige Isolierung (Schrumpfschlauch) nötig denn ein Kurzschluß hier (gerne als Wackelkontakt) hätte fatale Folgen.

Damit das Anlöten reibungslos klappt müssen die Adern sorgfältig vorbereitet werden. Zuerst werden an beiden Enden ca. 2 mm der Isolation abgezogen. Anschließend werden die dünnen Drähte zu einem winzigen Seil verdrillt. Das geht am besten, wenn man die feinen Drähte in einer Hand zwischen Daumen und Zeigefinger nach unten hängend festhält und mit der anderen Hand das ganze Drahtstück auf kurzer Länge zwischen Daumen und Zeigefinger in eine Richtung rollt (Geste des Geldzählens). Hat man auf diese Weise alle Enden verdrillt, werden alle Enden mit dem Lötkolben verzinnt. 

Da ein optimal passender USB-Stecker derzeit nicht zu bekommen war musste ein USB-Stecker etwas umgebaut werden. Die beiden inneren Anschlüsse sind miteinander verbunden. Das ist bei USB-Anschlüssen die nur zur Stomversorgung dienen so vorgesehen.

Das fertige Akkukabel wird in die Bodenwanne eingebaut. Der USB-Stecker wurde dazu mit reichlich Heißkleber in die Nut am Ende der Akkuaufnahme eingeklebt. Am einfachsten geht das, wenn man den Stecker in den Akku steckt und dann den Akku einbaut. Man trägt reichlich Heißkleber auf die Nut in der Bodenwanne auf und drückt den Akku bis zum Anschlag nach rechts und unten in die richtige Position, Dadurch ladet der Stecker genau dort wo er hingehört. Man läßt den Kleber etwas abkühlen und kann dann noch etwas nacharbeiten. Durch den häufigen Akkuwechsel wird der Stecker besonders beansprucht so dass ein fester Halt hier unbedingt notwendig ist! Also lieber etwas mehr Heißkleber verwenden als zu wenig.

Bitte darauf achten, dass kein Kleber an den Akku gerät und auch nicht in die Lücke zwischen Akku und dem Anschlag! Der Akku soll links ein Stück aus der Wanne ragen damit man den Akku leicht rausziehen kann. Der Platz rechts neben dem Akku wird als Weg für die Kabel zum Bug gebraucht.
