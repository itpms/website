---
title: "Fehler und mögliche Ursachen"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 80
---

## Nichts geschieht (auch die LED bleibt aus)

USB-Verbindung in Ordnung?

PC / Laptop erkennt den Roboter?

## LED blinkt dauernd

Richtiges Programm auf das Prozessorboard geladen?

## LED blinkt drei Mal aber Motoren drehen sich nicht

Verbindung von der Sammelschiene zum Motortreiber in Ordnung?

Falsche Pins am Prozessorboard verwendet?

## Ein Motor dreht nicht

Verbindungen vom Prozessorboard zum Motortreiber in Ordnung?

Verbindungen vom Motortreiber zum Motor in Ordnung?

## Motor dreht in die falsche Richtung

Anschlüsse des Motors am Motortreiber sind vertauscht.

## Motor läuft nicht langsam an sondern plötzlich

Verbindungen vom Prozessorboard zum Motortreiber vertauscht ?

Falsche Pins am Prozessorboard verwendet ?

## Motor läuft schwergängig / ungleichmäßig

Schleift die Antriebswelle in der Durchführung an der Bodenwanne?

Motor korrekt eingebaut?

Kabel korrekt verlöter und verbunden?

## Erst dreht der rechte Motor, dann der linke

Roboter steht richtig rum (Bug zeigt von uns weg)?

Anschlüsse „MOTOR-A“ mit „MOTOR-B“ am Motortreiber vertauscht ?

## Motoren laufen scheinbar wild durcheinander

Verbindungen vom Prozessorboard zum Motortreiber vertauscht ?

Falsche Pins am Prozessorboard verwendet ?
