---
title: "Montage des Antriebs"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 60
---

## Antrieb gelb

Beim „Antrieb gelb“ befinden sich die Motoren außen an der Bodenwanne weil in der Bodenwanne der Platz sonst knapp wird. Für Antriebswelle, Halteschrauben und Anschlüsse sind entsprechende Öffnungen vorhanden. Die rechteckigen Öffnungen für die Anschlüsse sind mit jeweils zwei Streben abgestützt um den 3D-Druck zu vereinfachen. Diese beiden Streben müssen vor der Montage entfernt werden. Die Motoren werden mit drei Schrauben an der Wanne festgeschraubt. Bei der dritten Schraube muss ein Distanzstück untergelegt werden damit die Kunststofföse nicht bei erster Gelegenheit abbricht. Die Motorbefestigung muss ziemlich stabil sein, damit der Roboter auch mal einen Sturz vom Tisch schadlos übersteht.

{{< alert context="warning" text="Wichtig sind die Distanzstücke (blau dargestellt). Ohne diese bricht das Befestigungsauge bei nächster Gelegenheit ab oder der Motor „hängt“ nur an zwei Schrauben ..."/>}}

![](MotoröffnungmitStützen.png "Motoröffnung mit Stützen")
![](Stützenentfernt.png "Stützen entfernt")
![](Motormontiert.png "Schaubild 1: Motor montiert")
![](WannemitMotoren.png "Wanne mit Motoren")

## Antrieb „klein“

Beim „Antrieb klein“ sitzen die Motoren innerhalb der Bodenwanne. Die Motoren werden so platziert, dass das Getriebe genau bündig mit der Wanne abschließt. Der Motor wird dann mit dem mitgelieferten Haltebügel in der Bodenwanne festgeschraubt. Außen werden kleine Distanzhülsen (ca 4mm) über die Antriebswellen geschoben damit die Räder nicht an der Bodenwanne schleifen. Als letztes werden die Räder mit den „Speichen“ nach außen auf die Wellen geschoben. Wir haben uns entscheiden, die Getriebe nicht wie vom Hersteller vorgesehen im Rad zu platzieren da das Getriebe dann außen liegt und binnen Minuten voller Flusen und Krümel ist.

![](KleinMotormontiert.png "Motor montiert")
![](AchsemitDistanzhülse.png "AchsemitDistanzhülse")
![](MontiertesRad.png "Montiertes Rad")
![](WannemitAntriebklein.png 'Wanne mit Antrieb "klein"')
