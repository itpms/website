---
title: "Systemsteckerleitung(en)"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 40
---

Nach dem wir jetzt so schön in Fahrt sind können wir gleich mit einer Systemsteckerleitung weitermachen. Wie verwenden von dem 10-poligen Stecker nur sieben Anschlüsse.

![](Systemsteckerleitung.jpg "Systemsteckerleitung")


1. Zuerst schneiden wir ein ca 25 cm langes Stück vom adernspendenden Netzwerkkabel ab, entfernen vorsichtig den Mantel, das Drahtgeflecht und evtl. vorhandene Kunststofflagen und haben die einzelnen Adern vor uns.
2. Die einzelnen Adern werden entsprechend dem Farbschema an den Wannenstecker gelötet. Dabei daran denken, dass die Bilder weiter oben „auf die Steckerseite schauen“. Wir schauen aber jetzt von der Lötseite aus auf den Stecker! Das klingt einfach, führt aber sogar bei den alten Hasen oft genug zu Fehlern.
3. Bevor wir jetzt die Einzelstecker anlöten schieben wir auf jede Ader ein ca. 5 mm langes Stück Schrumpfschlauch. Dann werden die Einzelstecker angelötet. Das geht am einfachsten, wenn wir sieben Stecker von einer Steckerleiste abschneiden, in der „Helfenden Hand“ festklemmen und dann Ader für Ader anlöten. Die Adern rt (5V), gn (Gnd) und or (3,3V)  sollten dabei in dieser Reihenfolge direkt nebeneinander angelötet werden. Sind alle Adern angelötet können die Schrumpschläuche über die Lötstellen geschoben werden und mit dem Lötkolben (oder einem Haarfön) vorsichtig erwärmt werden bis sich der Schrumpfschlauch zusammenzieht. Danach können die Einzelstecker voneinander getrennt werden wobei die „drei“ (rt, gn und or) zusammenbleiben.
4. Zum Abschluß wird auf dem Wannenstecker aus Heißkleber ein schöner Träger geformt der den Adern auch auf dieser Seite festen Halt gibt.

