---
title: "Sammelschiene"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 30
---

1. Aus dem Material der Streifenrasterplatine einen Streifen von ca 30 mm x 10 mm aussägen wobei die Kupferstreifen in Längsrichtung verlaufen. Der Streifen hat elf Löcher in Längsrichtung und drei Löcher in Querrichtung.<br>
![](Rasterplatine(Bauteilseite).png "Rasterplatine (Bauteilseite)") 
![](Rasterplatine(Lötseite).png "Rasterplatine (Lötseite)")
2. Von der Buchsenleiste drei Elemente mit jeweils neun Anschlüssen zuschneiden. Dabei genau in der Mitte des zehnten Anschlusses schneiden. Der zehnte Anschluss geht dabei verloren, insgesamt werden alsi dreißig Anschlüsse „verbraucht“. Die überstehenden Reste des zehnten Anschlusses werden bündig abgefeilt.
<br>![](Buchsen-leiste.png "Buchsenleiste")
3. Die drei Leisten so in das Platinenstück stecken, dass links und rechts je ein Loch frei bleibt. Die Konstruktion dann vorsichtig auf dem Kopf stellen dass die Kupferseite nach oben zeigt.
<br>![](AufgesteckteBuchsenleisten.png "Aufgesteckte Buchsenleisten")
4. Alle Beinchen der Buchsenleisten mit der Platine verlöten. Dabei dürfen in Querrichtung auf keinen Fall Verbindungen zwischen den Beinchen zurückbleiben.
<br>![](VerlöteteBuchsenleisten.png "Verlötete Buchsenleisten")
![](Sammelschiene.jpg "Sammelschiene")
