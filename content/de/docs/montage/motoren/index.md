---
title: "Motoren"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 50
---

Neben dem Anschlußkabel (10cm) muss je ein Entstörkondensator an den Motor angelötet werden. Ohne diesen Kondensator kann es zu Störungen der Bluetooth- oder WiFiverbindungen kommen. Leider nicht nur bei unserem Roboter selbst, sondern auch bei Robotern in der Nähe.
An das andere Kabelende werden genau wie beim Systemstecker Einzelstecker angelötet und natürlich mit Schrumpfschlauch geschützt. Auch diese Stecker dürfen gerne zusammenbleiben.
Grade beim „Antrieb gelb“ müssen die Kondensatoren so platziert werden dass sie bei der Montage nicht im Weg sind.

![](MotorimLieferzustand.png "Motor im Lieferzustand")
![](Motorfertigverdrahtet.png "Motor fertig verdrahtet")
![](MotormitKondensator.png "Motor mit Kondensator")
![](KleinerMotormitKondensator.png "Kleiner Motor mit Kondensator")
