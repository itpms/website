---
title: "Boards"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 10
---

Sofern die Boards und Motortreiber keine Steckerleiste haben müssen die Steckerleisten selbst angelötet werden. Häufig werden die Leisten mitgeliefert ansonsten sollten die Steckerleisten aus der Materialliste auch ausreichen. Ich bestelle die Boards immer „ohne“ weil der Aufpreis z.T. erheblich ist. Mit etwas Übung dauert das Anlöten keine Minute.

Beim Motortreiber werden Buchsenleisten verwendet (wie bei der Sammelschiene). Die Buchsenleisten werden auf der Oberseite (neben den silbernen Zylindern) eingebaut damit dort direkt die Kabel aufgesteckt werden können.

Bei den Prozessorboards hingegen werden Steckerleisten auf der Unterseite montiert damit die Boards in das Breadboard gesteckt werden können.

![](prozessorboard.png "Prozessorboard")
![](motortreiber.png "Motortreiber")
