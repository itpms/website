---
title: "Programm auf das Prozessorboard laden"
lead: ""
date: 2020-10-06T08:49:15+00:00
lastmod: 2020-10-06T08:49:15+00:00
images: ["usb.png"]
draft: false
weight: 60
---

Bevor irgendetwas an das Prozessorboard angeschlossen wird muss das passende Programm hochgeladen werden. Ein „altes“ Programm auf dem Board könnte mit dem Roboter unverträglich sein und das Prozessorboard könnte sogar Schaden nehmen.
Vor der Fortsetzung der Montage das Arduino-Programm „Roboter1_Motortest“ auf das Board hochladen.
Details sind in der Anleitung „Das erste Arduino-Programm“ beschrieben.
