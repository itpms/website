---
title: "Adernfarben und Steckverbinder"
description: "."
lead: ""
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "aufbauanleitung"
weight: 650
toc: true
---

{{< alert context="warning" text="Wichtig ist, dass Drähte (Adern) die den gleichen Zweck haben auch immer die gleiche Farbe haben und dass wenn irgend möglich ein einheitliches Farbschema verwendet wird. " />}}

Systemstecker 6 pol<br>![](systemstecker-6-pol.png "Systemstecker")<br>
Systemstecker 10 pol<br>![](systemstecker-10-pol.png)

  |Zweck / Name   |Farbkürzel   |Farbe
  |-------------- |------------ |---------
  |Gnd (Masse)    |gn           |grün
  |5 V            |rt           |rot
  |3,3 V          |or           |orange
  |Eingang 1      |gr           |grau
  |Eingang 2      |ge           |gelb
  |Eingang 3      |bl           |blau
  |Ausgang 1      |sw           |schwarz
  |Ausgang 2      |ws           |weiß

Sollten abweichende Aderfarben zur Verfügung stellen sollte jede:r
selber diese Dokumentation vor Beginn der Bausphase anpassen.

Anfängern rate ich, zu Beginn erst *einen* Systemstecker mit dem
Prozessorboard zu verbinden und weitere Stecker erst nach und nach bei
Erweiterung des Roboters zuzufügen. Das „Durcheinander" der vielen Adern
vervielfacht am Anfang Verdrahtungsfehler und erschwert die Fehlersuche
(auch für erfahrene Tinker!) ungemein.
