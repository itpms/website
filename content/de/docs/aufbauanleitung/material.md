---
title: "Material"
description: "."
lead: ""
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
menu:
  docs:
    parent: "Aufbauanleitung"
weight: 630
toc: true
---
![images](usb.png)
|Anzahl| Material | Marktpreis (gemittelt)
|---|---|---|
|1(\*) |Prozessorboard „Arduino Nano“   | 11,00€  | 
|1(\*) |Prozessorboard „Arduino Teensy“   | 24,00€  | 
|1     |Breadboard 17x10   | 3,00€  | 
|1     |Jumperkabel (1Set = 40St)   | 1,50€  | 
|1     |Motortreiber   | 1,95€  | 
|1(\*) |Motor „Gelb“ inkl Rad (grade Ausführung)   | 2 x 3,80€  | 
|1(\*) |Motor „klein“ inkl Rad   | 2 x 3,80€  | 
|1     |Akku VARTA Powerpack 57959   | 13,00€  | 
|1     |USB-Kabel passend zu P-Board   | 3,50€  | 
|1     |HC-05 Bluetoothmodul   | 11,00€  | 
|10    |Wannenstecker   | 10 x 0,10€ | 
|1     |2m Flachbandkabel min 10-polig | 2 x 1,30€ | 
|10    |Pfostenstecker | 10 x 0.12€ | 
|1     |USB-Stecker mit Lötanschlüssen | 1,25€ | 
|2     |Buchsenleiste 40 polig (wird passend zugeschnitten) | 2 x 0,80€ | 
|2     |Steckerleiste 40 polig (wird passend zugeschnitten) | 2 x 0,80€ | 
|1     |Schrumpfschlauch 40 cm (Passend auf die Kabeladern) | 0,40€ | 
|1     |1,5 m Netzwerkkabel (Spender für die Kabeladern) | 3,00€ | 
|8     |„Holz“-Schrauben 2,2 mm x 5 mm | 8 x 0,05€ | 
|5     |Metrische Schrauben M2 x 6 inkl. Mutter und Unterlegscheibe | 5 x 0,05€ | 
|2     |Entstörkondensatoren 100nF | 2 x 0,08€ | 
|1     |Bodengleiter (kann auch selbst gedruckt werden) | 3,50€ | 
|1     |Streifenrasterplatine (es reicht ca 1,5 x 5 cm).Allerdings werden diese Plantinen meist nur im „Postkartenformat“ angeboten | 3,50€ | 

\*) wahlweise

Das Kleinmaterial ist großzügig bemessen da ja durchaus etwas bei der Montage kaputtgehen kann.	

Bezugsquellen
 (ohne Anspruch auf Vollständigkeit)
- [Roboter Bausatz](www.roboter-bausatz.de)
- [Polin](www.pollin.de)
- [Reichelt](www.reichelt.de)
- [Conrad](www.conrad.de)
- [Marotronics](www.marotronics.de)
- [Klose-MS](www.klose-ms.de)
- und natürlich die großen Onlineauktionshäuser und -schuhhändler (daher auch die umfangreichen Fotos)
