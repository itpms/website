---
title: "3D-Druck"
description: "."
lead: ""
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "aufbauanleitung"
weight: 700
toc: true
---

Beim Ausdruck der 3D-Modelle rate ich zu folgender Reihenfolge: 
1. Inlet (und für „Antrieb Gelb") 2 x Distanzstück 
2. Bodenwanne. Dabei die „richtige" Wanne
entsprechend dem gewünschten Antrieb („gelb" oder „klein") auswählen 
3. Bug 
4. Bugdeckel
5. Wannendeckel
6. ggf. Bodengleiter (Halbkugel)
